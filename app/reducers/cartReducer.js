import {
  ADD_CART_ITEM,
  RESET_CART,
  REMOVE_CART_ITEM
} from '../actions/cartActions';

const _initialState = [];

const cartReducer = (state = _initialState, action) => {
  switch(action.type) {
    case ADD_CART_ITEM:
      return [ ...state, action.item ];
    case RESET_CART:
      return _initialState;
    case REMOVE_CART_ITEM:
      return [
        ...state.slice(0, action.idx),
        ...state.slice(action.idx + 1)
      ];
    default:
      return state;
  }
}

export default cartReducer;
