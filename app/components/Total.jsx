import React from 'react';

const Total = ({ cart }) => {
  let prices = cart.map(item => item.price);
  let totalPrice = prices.reduce((acc, val) => {
    return acc + val;
  }, 0).toFixed(2);

  return (
    <div className="total-container">
      <div className="total">
        ${totalPrice}
      </div>
    </div>
  );
};

export default Total;
