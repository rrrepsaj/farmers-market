import React, { Component } from 'react';
import { connect } from 'react-redux';

import { removeCartItem } from '../actions/cartActions';

class RemoveItemButton extends Component {
  constructor(props) {
    super(props);

    this.onRemoveCartItem = this.onRemoveCartItem.bind(this);
  }

  onRemoveCartItem(e) {
    e.preventDefault();

    const { idx, removeCartItem } = this.props;
    removeCartItem(idx);
  }

  render() {
    return (
      <div>
        <i className="fa fa-trash-o fa-lg remove-item-button" onClick={this.onRemoveCartItem} aria-hidden="true"></i>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  removeCartItem: (idx) => dispatch(removeCartItem(idx))
});

export default connect(null, mapDispatchToProps)(RemoveItemButton);
