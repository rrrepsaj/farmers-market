import React from 'react';
import { connect } from 'react-redux';

import { getDiscountedItems } from '../selectors/discounts';

import CartList from './CartList';
import Total from './Total';

const Cart = ({ cart }) => (
  <div className="cart">
    <span><h3>Cart</h3></span>
    <CartList cart={cart} />
    <Total cart={cart} />
  </div>
);

const mapStateToProps = (state) => ({
  cart: getDiscountedItems(state.cart)
});

export default connect(mapStateToProps)(Cart);
