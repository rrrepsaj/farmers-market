import React, { Component } from 'react';

import CartListItem from './CartListItem';
import Total from './Total';

class CartList extends Component {
  constructor(props) {
    super(props);
  }

  renderBOGO() {
    return <span>BOGO!!</span>;
  }

  render() {
    const { cart } = this.props;

    return (
      <div className="cart-list">
        <ul>
          {cart.map((item, idx) => <CartListItem key={idx} idx={idx} item={item} cart={cart} />)}
        </ul>
      </div>
    );
  }
}

export default CartList;
