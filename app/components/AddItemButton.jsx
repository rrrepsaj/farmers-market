import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addCartItem } from '../actions/cartActions';

class AddItemButton extends Component {
  constructor(props) {
    super(props);

    this.onAddCartItem = this.onAddCartItem.bind(this);
  }

  onAddCartItem(e) {
    e.preventDefault();

    const { item, addCartItem } = this.props;
    addCartItem(item);
  }

  render() {
    return (
      <a href="#" className="add-item-button" onClick={this.onAddCartItem}>+</a>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  addCartItem: (item) => dispatch(addCartItem(item))
});

export default connect(null, mapDispatchToProps)(AddItemButton);
