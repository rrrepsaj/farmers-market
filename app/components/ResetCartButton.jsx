import React, { Component } from 'react';
import { connect } from 'react-redux';

import { resetCart } from '../actions/cartActions';

class ResetCartButton extends Component {
  constructor(props) {
    super(props);

    this.onResetCart = this.onResetCart.bind(this);
  }

  onResetCart(e) {
    e.preventDefault();

    this.props.resetCart();
  }

  render() {
    return (
      <a href="#" className="reset-cart-button" onClick={this.onResetCart}>Reset Cart</a>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  resetCart: () => dispatch(resetCart())
});

export default connect(null, mapDispatchToProps)(ResetCartButton);
