import React from 'react';
import { connect } from 'react-redux';

import AddItemButton from './AddItemButton';

const ProductListItem = ({ item }) => {
  const { code, name, price, discount } = item;
  const itemLabel = `${code} - ${name}`;
  const itemPrice = `$${price.toFixed(2)}`;

  return (
    <li>
      <span className="button-name-group">
        <span className="add-button">
          <AddItemButton item={item} />
        </span>
        <span className="product-name">
          {itemLabel}
          <span className="product-discount">{discount}</span>
        </span>
      </span>
      <span>{itemPrice}</span>
    </li>
  );
};

export default ProductListItem;
