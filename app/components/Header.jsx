import React from 'react';

const Header = () => (
  <div>
    <div className="header">
      <nav>
        <img src="images/slg_logo.png" alt="Student Loan Genius logo" />
      </nav>
    </div>
  </div>
);

export default Header;
