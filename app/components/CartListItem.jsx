import React, { Component } from 'react';

import RemoveItemButton from './RemoveItemButton';

class CartListItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { code, name, price } = this.props.item;

    const itemLabel = `${code} - ${name}`;
    const itemPrice = `$${price.toFixed(2)}`;

    return (
      <li>
        <span>
          <span className="cart-remove-button"><RemoveItemButton idx={this.props.idx} /></span>
          <span className="cart-item-label">{itemLabel}</span>
        </span>
        <span className="cart-item-quantity">1</span>
        <span className="cart-item-price">{itemPrice}</span>
      </li>
    );
  }
}

export default CartListItem;
