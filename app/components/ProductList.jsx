import React from 'react';

import ProductListItem from './ProductListItem';
import ResetCartButton from './ResetCartButton';

const ProductList = ({ products }) => (
    <div className="product-list">
      <h2>Available Products</h2>
      <ul>
        {products.map((product, idx) => <ProductListItem key={idx} item={product} />)}
      </ul>
      <ResetCartButton />
    </div>
);

export default ProductList;
