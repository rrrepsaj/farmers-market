import React from 'react';

import Header from './Header';
import ProductList from './ProductList';
import Cart from './Cart';

const PRODUCTS = [
  {
    code: "CH1",
    name: "Chai",
    price: 3.11,
    discount: "Purchase a box of chai and get your first milk free!"
  },
  {
    code: "AP1",
    name: "Apples",
    price: 6.00,
    discount: "Only $4.50 when buying 3 or more!"
  },
  {
    code: "CF1",
    name: "Coffee",
    price: 11.23,
    discount: "BOGO - Buy One Get One Free!"
  },
  {
    code: "MK1",
    name: "Milk",
    price: 4.75,
    discount: "First milk free with a purchase of a box of chai!"
  }
];

const Root = ({ store }) => (
  <div>
    <Header />
    <div className="store">
      <ProductList products={PRODUCTS} />
      <Cart />
    </div>
  </div>
);

export default Root;
