export const getDiscountedItems = (cart) => {
  const discountedItems = JSON.parse(JSON.stringify(cart));

  discountedItems.forEach((item, idx) => {
    item.id = idx;
  });

  let chais = discountedItems.filter(item => item.name === "Chai");
  let apples = discountedItems.filter(item => item.name === "Apples");
  let coffees = discountedItems.filter(item => item.name === "Coffee");
  let milks = discountedItems.filter(item => item.name === "Milk");

  // CHMK
  if (milks.length) {
    milks[0].price = chais.length ? 0.00 : 4.75;
  }

  // BOGO
  if (coffees.length) {
    coffees.forEach(coffee => {
      coffee.price = coffees.indexOf(coffee) % 2 === 0 ? 11.23 : 0.00;
    });
  }

  // APPL
  if (apples.length) {
    apples.forEach(bag => {
      bag.price = apples.length >= 3 ? 4.50 : 6.00;
    });
  }

  let result = [ ...chais, ...apples, ...coffees, ...milks ];

  result.sort((a, b) => a.id - b.id);
  return result;
};
