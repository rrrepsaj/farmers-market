import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './store/store';
import Root from './components/Root';

require('./styles/app.scss')

const store = configureStore();

const App = () => (
  <Provider store={store}>
    <Root />
  </Provider>
);

document.addEventListener("DOMContentLoaded", () => {
  const root = document.getElementById("root");
  ReactDOM.render(<App />, root);
});
