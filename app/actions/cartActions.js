export const ADD_CART_ITEM = "ADD_CART_ITEM";
export const RESET_CART = "RESET_CART";
export const REMOVE_CART_ITEM = "REMOVE_CART_ITEM";

export const addCartItem = (item) => ({
  type: ADD_CART_ITEM,
  item
});

export const resetCart = () => ({
  type: RESET_CART
});

export const removeCartItem = (idx) => ({
  type: REMOVE_CART_ITEM,
  idx
});
